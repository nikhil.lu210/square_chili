<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $fillable = [
        'full_name',
        'date',
        'time',
        'amount',
        'email',
        'phone',
        'message'
    ];
}
