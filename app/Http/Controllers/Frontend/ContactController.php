<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Contact;
use Session;
use Mail;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('frontend.contact.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name'=>'required',
            'last_name'=>'required',
            'email'=>'required',
            'subject'=>'required'
        ]);

        $contact = new Contact();        
        $contact->first_name = $request->first_name;
        $contact->last_name = $request->last_name;
        $contact->email = $request->email;
        $contact->subject = $request->subject;
        $contact->message = $request->message;
        // dd($contact);
        $contact->save();


        // For Getting Mail From Contact Page
        $mail_send_data = [
            'name' => $request->first_name. " " .$request->last_name,
            'email' => $request->email,
            'subject' => $request->subject,
            'msg' => $request->message
        ];

        Mail::send('partials.email.contactEmail', $mail_send_data, function($message) use ($mail_send_data) {
			$message->to('nikhil.lu210@gmail.com');
			$message->subject($mail_send_data['subject']);
			$message->from('monopuras5@gmail.com', 'Square Chili');
	   });


        // Auto Reply From The Company
        $mail_reply_data = [
            'name' => $request->first_name. " " .$request->last_name,
            'email' => $request->email,
            'subject' => $request->subject
        ];

        Mail::send('partials.email.contactReplyMail', $mail_reply_data, function($message) use ($mail_reply_data) {
			$message->to($mail_reply_data['email']);
			$message->subject($mail_reply_data['subject']);
			$message->from('monopuras5@gmail.com', 'Square Chili');
	   });

        Session::flash('success', 'Message Sent.');
        return redirect()->back()->withSuccess( 'Message Sent Successfully.' );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('frontend.contact.show')->with('contacts', Contact::all());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
