<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Reservation;
use Session;
use Mail;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('frontend.reservation.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'full_name'=>'required',
            'date'=>'required',
            'time'=>'required',
            'amount'=>'required',
            'email'=>'required',
            'phone'=>'required',
            'message'=>'required'
        ]);
        // dd($request);

        $reservation = new Reservation();        
        $reservation->full_name = $request->full_name;
        $reservation->date = $request->date;
        $reservation->time = $request->time;
        $reservation->amount = $request->amount;
        $reservation->email = $request->email;
        $reservation->phone = $request->phone;
        $reservation->message = $request->message;
        // dd($reservation);
        $reservation->save();


        // For Getting Mail From reservation Page
        $mail_send_data = [
            'name' => $request->full_name,
            'date' => $request->date,
            'time' => $request->time,
            'amount' => $request->amount,
            'email' => $request->email,
            'phone' => $request->phone,
            'msg' => $request->message
        ];

        Mail::send('partials.email.reservationEmail', $mail_send_data, function($message) use ($mail_send_data) {
			$message->to('nikhil.lu210@gmail.com');
			$message->subject('New Reservation on'. ' ' .$mail_send_data['date']. ' ' . 'at' .$mail_send_data['time'] );
			$message->from('monopuras5@gmail.com', 'Square Chili');
	   });


        // Auto Reply From The Company
        $mail_reply_data = [
            'name' => $request->full_name,
            'email' => $request->email
        ];

        Mail::send('partials.email.reservationReplyMail', $mail_reply_data, function($message) use ($mail_reply_data) {
			$message->to($mail_reply_data['email']);
			$message->subject('Reservation Confirmation');
			$message->from('monopuras5@gmail.com', 'Square Chili');
	   });

        // Session::flash('alert-success', 'Reservation Request Sent Successfully.');
        return redirect()->back()->withSuccess( 'Reservation Request Sent Successfully.' );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('frontend.reservation.show')->with('reservations', Reservation::all());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
