<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('frontend.home.index');
// });
Route::get('/', 'Frontend\HomeController@index')->name('home');
Route::get('/about', 'Frontend\AboutController@index')->name('about');
Route::get('/gallery', 'Frontend\GalleryController@index')->name('gallery');
Route::get('/reservation', 'Frontend\ReservationController@index')->name('reservation');
Route::get('/contact', 'Frontend\ContactController@index')->name('contact');
Route::post('/contact/store', 'Frontend\ContactController@store')->name('contact.store');
Route::post('/reservation/store', 'Frontend\ReservationController@store')->name('reservation.store');

Auth::routes();

// Admin Dashboard
Route::get('/admin', 'Admin\HomeController@index')->name('admin');

// Admin Contact
Route::get('/admin/contact', 'Admin\ContactController@index')->name('admin.contact');
Route::get('/admin/contact/show/{id}', 'Admin\ContactController@show')->name('contact.show');
Route::get('/admin/contact/destroy/{id}', 'Admin\ContactController@destroy')->name('contact.destroy');
Route::get('/admin/contact/downloadExcel/{type}', 'Admin\ContactController@downloadExcel')->name('contact.download');

// Admin Reservation
Route::get('/admin/reservation', 'Admin\ReservationController@index')->name('admin.reservation');
Route::get('/admin/reservation/show/{id}', 'Admin\ReservationController@show')->name('reservation.show');
Route::get('/admin/reservation/destroy/{id}', 'Admin\ReservationController@destroy')->name('reservation.destroy');
Route::get('/admin/reservation/downloadExcel/{type}', 'Admin\ReservationController@downloadExcel')->name('reservation.download');