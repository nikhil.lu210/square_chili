<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin Login</title>
    <link rel="stylesheet" href="{{asset('backend/js/vendor/simple-line-icons/css/simple-line-icons.css')}}">
    <link rel="stylesheet" href="{{asset('backend/js/vendor/font-awesome/css/fontawesome-all.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/css/all.css')}}">
    <link rel="stylesheet" href="{{asset('backend/css/style.css')}}">
</head>
<body class="login-body">

<div class="page-wrapper flex-row align-items-center">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-5 login-card">
                
                <div class="card">
                    <div class="card-header bg-light"><strong>Admin Registration</strong></div>
                    <div class="card-body py-5">
                        <div class="text-center" style="text-transform: uppercase; font-size: 20px;"><strong>Admin Registration Not Allowed</strong></div>
                    </div>
                    <div class="card-footer bg-light" style="padding: 5px;">
                        <a href="{{Route('login')}}" class="btn btn-outline-dark btn-lg btn-block"><strong>Back to Login Page</strong></a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script src="{{asset('backend/js/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('backend/js/vendor/popper.js/popper.min.js')}}"></script>
<script src="{{asset('backend/js/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('backend/js/vendor/chart.js/chart.min.js')}}"></script>
<script src="{{asset('backend/js/carbon.js')}}"></script>
<script src="{{asset('backend/js/demo.js')}}"></script>
<script src="{{asset('backend/js/script.js')}}"></script>
</body>
</html>