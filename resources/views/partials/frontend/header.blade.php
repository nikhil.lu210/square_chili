
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no" /> 
    {{-- <!-- CSRF Token -->--}}
    <meta name="csrf-token" content="{{ csrf_token() }}"> 
    
    {{--  Page Title  --}}
    <title> Square Chilli @yield('page_title') </title>

    {{-- Icon in Tab --}}
    <link rel="shortcut icon" href="{{ asset('frontend/images/logo.png') }}"> 
    <style id="dynamic-css"></style>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/swiper.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/style.css') }}"/>
    
    @yield('stylesheet')
