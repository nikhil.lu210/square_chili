{{--  <!-- ========< Footer Section Starts >=============== -->  --}}
<footer class="footer footer-style-1 type-2">
    <div class="empty-sm-60 empty-xs-40"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-sm-12">
                <div class="footer-item">
                    <img src="{{ asset('frontend/images/logo.png') }}" alt="">
                    <div class="empty-sm-15 empty-xs-15"></div>
                    <div class="simple-text">
                        <p>Square chili Restaurant is located on 2 Mount ST, Manchester M2 5WQ in UK. We serve the finest Indian Cuisine that will surely give everyone an amazing food experience.</p>
                    </div>
                    <div class="empty-sm-20 empty-xs-20"></div>
                    <div class="follow follow-style-2">
                        @include('partials.frontend.social')
                    </div>
                </div>
                <div class="empty-sm-30 empty-xs-30"></div>
            </div>
            <div class="col-md-3 col-sm-6 col-sm-12">
                <div class="footer-item">
                    <h5 class="h5">Contact Us</h5>
                    <hr>
                    <div class="empty-sm-15 empty-xs-15"></div>
                    <ul class="list-style-2 ul-list">
                        <li>2 Mount ST,
                            Manchester M2 5WQ, 
                            United Kingdom</li>
                        <li><a href="tel:+44 161 819 1187" class="link-hover">+44 161 819 1187</a></li>
                        <li><a href="mailto:info@squarechili.co.uk" class="link-hover">info@squarechili.co.uk</a></li>
                    </ul>
                </div>
                <div class="empty-sm-30 empty-xs-30"></div>
            </div>
            <div class="clearfix hidden-xs visible-sm hidden-md hidden-lg"></div>
            <div class="col-md-2 col-sm-6 col-sm-12">
                <div class="footer-item">
                    <h5 class="h5">quick links</h5>
                    <hr>
                    <div class="empty-sm-15 empty-xs-15"></div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <ul class="list-style-3 ul-list">
                                <li><a href="#" class="link-hover">HOME</a></li>
                                <li><a href="#" class="link-hover">ABOUT</a></li>
                                <li><a href="#" class="link-hover">GALLARY</a></li>
                                <li><a href="#" class="link-hover">RESERVATION</a></li>
                                <li><a href="#" class="link-hover">CONTACT</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="empty-sm-30 empty-xs-30"></div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="footer-item">
                    <h5 class="h5">Opening Hours</h5>
                    <hr>
                    <div class="empty-sm-15 empty-xs-15"></div>
                    <div class="recent-post">
                        <table class="working-day" width="100%">
                            <tbody>
                                <tr style="color: #898989;">
                                    <th style="text-align: left !important; width:40%;" class="fw_700">Tuesday :</th>
                                    <td style="text-align: right !important;">04.30 pm - 10.30 pm</td>
                                </tr>
                                <tr style="color: #898989;">
                                    <th style="text-align: left !important; width:40%;" class="fw_700">Wednesday :</th>
                                    <td style="text-align: right !important;">04.30 pm - 10.30 pm</td>
                                </tr>
                                <tr style="color: #898989;">
                                    <th style="text-align: left !important; width:40%;" class="fw_700">Thrusday :</th>
                                    <td style="text-align: right !important;">04.30 pm - 10.30 pm</td>
                                </tr>
                                <tr style="color: #898989;">
                                    <th style="text-align: left !important; width:40%;" class="fw_700">Friday :</th>
                                    <td style="text-align: right !important;">04.30 pm - 11.30 pm</td>
                                </tr>
                                <tr style="color: #898989;">
                                    <th style="text-align: left !important; width:40%;" class="fw_700">Saturday :</th>
                                    <td style="text-align: right !important;">04.30 pm - 11.30 pm</td>
                                </tr>
                                <tr style="color: #898989;">
                                    <th style="text-align: left !important; width:40%;" class="fw_700">Sunday :</th>
                                    <td style="text-align: right !important;">03.30 pm - 09.00 pm</td>
                                </tr>
                                <tr style="color: #898989;">
                                    <th style="text-align: left !important; width:40%;" class="fw_700">Monday :</th>
                                    <td style="text-align: right !important;">Closed</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="empty-sm-30 empty-xs-30"></div>
            </div>
        </div>
        <div class="empty-sm-10 empty-xs-10"></div>
    </div>
    <div class="copyright text-center">
        <div class="empty-sm-20 empty-xs-20"></div>
        <div class="container">
            <span>© All Rights Reserved Square Chili | Development by <a href="https://www.veechitechnologies.com" target="_blank"><b>VEECHI TECHNOLOGIES</b></b></a></span>
        </div>
        <div class="empty-sm-20 empty-xs-20"></div>
    </div>
</footer>
{{--  <!-- ========< Footer Section Ends >=============== -->  --}}