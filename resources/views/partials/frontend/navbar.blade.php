<header class="header type-2 header-style-5 scrol">
    <div class="navigation flex-align">
        <a href="{{Route('home')}}" class="logo"><img src="{{asset('frontend/images/logo.png')}}" alt="delice"><img src="{{asset('frontend/images/logo.png')}}" alt="delice" class="logo-type-2"></a>
        <div class="burger-menu"><i></i></div>
        <div class="nav">
            <div class="nav-align-wrap">
                <div class="right-block">
                    <div class="follow">
                        @include('partials.frontend.social')
                    </div>
                </div>
                <nav>
                    <ul class="header-menu">
                        <li class="home"><a href="{{Route('home')}}">Home</a></li>
                        <li class="about"><a href="{{Route('about')}}">About</a></li>
                        <li class="gallery"><a href="{{Route('gallery')}}">Gallery</a></li>
                        <li class="reservation"><a href="{{Route('reservation')}}">Reservation</a></li>
                        <li class="contact"><a href="{{Route('contact')}}">Contact</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>