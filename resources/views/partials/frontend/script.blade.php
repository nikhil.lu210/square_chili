<script src="{{ asset('frontend/js/jquery.min.js') }}"></script>
<script src="{{ asset('frontend/js/swiper.jquery.min.js') }}"></script>
<script src="{{ asset('frontend/js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('frontend/js/jscolor.min.js') }}"></script>
<script src="{{ asset('frontend/js/jquery.knob.js') }}"></script>
<script src="{{ asset('frontend/js/jquery.throttle.js') }}"></script>
<script src="{{ asset('frontend/js/jquery.classycountdown.js') }}"></script>
<script src="{{ asset('frontend/js/jarallax.js') }}"></script>
<script src="{{ asset('frontend/js/all.js') }}"></script>
<script src="{{ asset('frontend/js/color.picker.js') }}"></script>

@yield('scripts')