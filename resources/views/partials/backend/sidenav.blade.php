<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-title">Navigation</li>

            <li class="nav-item">
                <a href="{{ Route('admin')}}" class="nav-link">
                    <i class="icon icon-speedometer"></i> Dashboard
                </a>
            </li>

            <li class="nav-item nav-dropdown">
                <a href="#" class="nav-link nav-dropdown-toggle">
                    <i class="icon icon-grid"></i> Records <i class="fa fa-caret-left"></i>
                </a>

                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="{{ Route('admin.contact') }}" class="nav-link">
                            <i class="icon icon-envelope-open"></i> Contacts
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{ Route('admin.reservation') }}" class="nav-link">
                            <i class="icon icon-envelope"></i> Reservations
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-title">More</li>

            <li class="nav-item nav-dropdown">
                <a href="#" class="nav-link nav-dropdown-toggle">
                    <i class="icon icon-game-controller"></i> Settings <i class="fa fa-caret-left"></i>
                </a>

                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="icon icon-game-controller"></i> Blank Page
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </nav>
</div>