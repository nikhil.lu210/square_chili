@extends('layouts.backend')

@section('page_title', '| RESERVATIONS')

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .card-header h6{
            font-weight: 600;
            margin-top: 3px;
            margin-bottom: 0px;
        }
    </style>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header bg-light">
                    <h6 class="name float-left"><strong>{{ $reservations->full_name }} Reservation on {{ $reservations->date }} at {{ $reservations->time }}</strong></h6>
                    <a onclick="history.back()" class="btn btn-light btn-sm float-right">Back</a>
                </div>
                <div class="card-body">
                    <table class="table table-striped table-bordered">
                        <tbody>
                            <tr>
                                <th style="text-align: left !important; width:30%;" class="fw_700">Name:</th>
                                <td style="text-align: left !important;">{{ $reservations->full_name }}</td>
                            </tr>
                            <tr>
                                <th style="text-align: left !important; width:30%;" class="fw_700">Date:</th>
                                <td style="text-align: left !important;">{{ $reservations->date }}</td>
                            </tr>
                            <tr>
                                <th style="text-align: left !important; width:30%;" class="fw_700">Time:</th>
                                <td style="text-align: left !important;">{{ $reservations->time }}</td>
                            </tr>
                            <tr>
                                <th style="text-align: left !important; width:30%;" class="fw_700">Total Person:</th>
                                <td style="text-align: left !important;">{{ $reservations->amount }}</td>
                            </tr>
                            <tr>
                                <th style="text-align: left !important; width:30%;" class="fw_700">Email:</th>
                                <td style="text-align: left !important;">{{ $reservations->email }}</td>
                            </tr>
                            <tr>
                                <th style="text-align: left !important; width:30%;" class="fw_700">Contact No:</th>
                                <td style="text-align: left !important;">{{ $reservations->phone }}</td>
                            </tr>
                            <tr>
                                <th style="text-align: left !important; width:30%;" class="fw_700">Message:</th>
                                <td style="text-align: left !important;">{{ $reservations->message }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
