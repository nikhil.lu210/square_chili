@extends('layouts.backend')

@section('page_title', '| RESERVATION')

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .btn-outline-dark:not(:disabled):not(.disabled).active:focus, 
        .btn-outline-dark:not(:disabled):not(.disabled):active:focus, 
        .show>.btn-outline-dark.dropdown-toggle:focus,
        .btn-outline-dark.focus, 
        .btn-outline-dark:focus{
            box-shadow: none;
        }
    </style>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header bg-light">
                    <b class="float-left">All Reservations</b>
                    <a href="{{ Route('reservation.download', ['csv'])}}" class="btn btn-outline-dark btn-sm float-right" target="_blank">
                        <strong>DOWNLOAD CSV</strong>
                    </a>
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Total Person</th>
                                <th>Email</th>
                                <th>Contact No</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach ($reservations as $reservation)
                                <tr>
                                    <td>{{ $reservation->id }}</td>
                                    <td class="text-nowrap">{{ $reservation->full_name }}</td>
                                    <td>{{ $reservation->date }}</td>
                                    <td>{{ $reservation->time }}</td>
                                    <td>{{ $reservation->amount }}</td>
                                    <td>{{ $reservation->email }}</td>
                                    <td>{{ $reservation->phone }}</td>
                                    <td>
                                        <a href="{{ Route('reservation.destroy', ['id'=>$reservation->id]) }}" onclick="return confirm('Delete! Are you sure?')" class="btn btn-light btn-sm"><i class="fas fa-trash"></i></a>
                                        <a href="{{ Route('reservation.show', ['id'=>$reservation->id])}}" class="btn btn-light btn-sm"><i class="fas fa-info"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
