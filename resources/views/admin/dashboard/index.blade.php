@extends('layouts.backend')

@section('page_title', '| DASHBOARD')

@section('stylesheet')
    {{--  External CSS  --}}
    
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-4">
            <div class="card p-4">
                <div class="card-body d-flex justify-content-between align-items-center">
                    <div>
                        <span class="h4 d-block font-weight-normal mb-2">{{ \App\Model\Admin\Reservation::all()->count('id') }}</span>
                        <span class="font-weight-light"><strong>TOTAL RESERVATION</strong></span>
                    </div>

                    <div class="h2 text-muted">
                        <i class="icon icon-envelope"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card p-4">
                <div class="card-body d-flex justify-content-between align-items-center">
                    <div>
                        <span class="h4 d-block font-weight-normal mb-2">{{ \App\Model\Admin\Contact::all()->count('id') }}</span>
                        <span class="font-weight-light"><strong>TOTAL MAIL</strong></span>
                    </div>

                    <div class="h2 text-muted">
                        <i class="icon icon-envelope-open"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card p-4">
                <div class="card-body d-flex justify-content-between align-items-center">
                    <div>
                        <span class="h4 d-block font-weight-normal mb-2">0</span>
                        <span class="font-weight-light"><strong>TOTAL VISITOR</strong></span>
                    </div>

                    <div class="h2 text-muted">
                        <i class="icon icon-eyeglass"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    
@endsection