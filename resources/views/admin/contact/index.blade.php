@extends('layouts.backend')

@section('page_title', '| CONTACTS')

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .btn-outline-dark:not(:disabled):not(.disabled).active:focus, 
        .btn-outline-dark:not(:disabled):not(.disabled):active:focus, 
        .show>.btn-outline-dark.dropdown-toggle:focus,
        .btn-outline-dark.focus, 
        .btn-outline-dark:focus{
            box-shadow: none;
        }
    </style>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header bg-light">
                    <b class="float-left">Contact Messages</b>
                    <a href="{{ Route('contact.download', ['csv'])}}" class="btn btn-outline-dark btn-sm float-right" target="_blank">
                        <strong>DOWNLOAD CSV</strong>
                    </a>
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th><b>No.</b></th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach ($contacts as $contact)
                                <tr>
                                    <td><b>{{$contact->id}}</b></td>
                                    <td class="text-nowrap">{{$contact->first_name}} {{$contact->last_name}}</td>
                                    <td>{{$contact->email}}</td>
                                    <td>{{$contact->created_at->format('Y-m-d')}}</td>
                                    <td>{{$contact->created_at->format('H:m:s')}}</td>
                                    <td>
                                        <a href="{{ Route('contact.destroy', ['id'=>$contact->id]) }}" onclick="return confirm('Delete! Are you sure?')" class="btn btn-light btn-sm"><i class="fas fa-trash"></i></a>
                                        <a href="{{ Route('contact.show', ['id'=>$contact->id])}}" class="btn btn-light btn-sm"><i class="fas fa-info"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
