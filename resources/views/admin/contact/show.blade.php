@extends('layouts.backend')

@section('page_title', '| CONTACTS')

@section('stylesheet')
    {{--  External CSS  --}}
    <style>
        .card-header h6{
            font-weight: 600;
            margin-top: 3px;
            margin-bottom: 0px;
        }
    </style>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header bg-light">
                    <h6 class="name float-left">Contact Messages</h6>
                    <a href="{{Route('admin.contact')}}" class="btn btn-light btn-sm float-right">Back</a>
                </div>
                <div class="card-body">
                    <table class="table table-striped table-bordered">
                        <tbody>
                            <tr>
                                <th style="text-align: left !important; width:30%;" class="fw_700">Name:</th>
                                <td style="text-align: left !important;">{{ $contacts->first_name }} {{ $contacts->last_name }}</td>
                            </tr>
                            <tr>
                                <th style="text-align: left !important; width:30%;" class="fw_700">Email:</th>
                                <td style="text-align: left !important;">{{ $contacts->email }}</td>
                            </tr>
                            <tr>
                                <th style="text-align: left !important; width:30%;" class="fw_700">Subject:</th>
                                <td style="text-align: left !important;">{{ $contacts->subject }}</td>
                            </tr>
                            <tr>
                                <th style="text-align: left !important; width:30%;" class="fw_700">Message:</th>
                                <td style="text-align: left !important;">{{ $contacts->message }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
