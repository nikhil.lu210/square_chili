<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no" /> 
    {{-- <!-- CSRF Token -->--}}
    <meta name="csrf-token" content="{{ csrf_token() }}"> 
    
    {{--  Page Title  --}}
    <title> Square Chilli @yield('page_title') </title>

    {{-- Icon in Tab --}}
    <link rel="shortcut icon" href="{{ asset('frontend/images/logo.png') }}"> 
    <style id="dynamic-css"></style>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/swiper.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/style.css') }}"/>
</head>

<body class="page-color-style-2 page-style-2">
        <header class="header header-style-2">
            <div class="navigation flex-align">
                <a href="{{Route('home')}}" class="logo"><img src="{{asset('frontend/images/logo.png')}}" alt="square-chili"><img src="{{asset('frontend/images/logo.png')}}" alt="square-chili" class="logo-type-2"></a>
                <div class="burger-menu"><i></i></div>
                <div class="nav">
                    <div class="nav-align-wrap">
                        <div class="right-block">
                            <div class="follow homepage">
                                @include('partials.frontend.social')
                            </div>
                        </div>
                        <nav>
                            <ul class="header-menu">
                                <li class="active"><a href="/">Home</a></li>
                                <li><a href="about">About</a></li>
                                <li><a href="gallery">Gallary</a></li>
                                <li><a href="reservation">Reservation</a></li>
                                <li><a href="contact">Contact</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </header>
    
        <div class="sub-header flex-align">
            <div class="container">
                <div class="fl">
                    <ul class="list-style-2 ul-list color-type-1">
                        <li>
                            <i class="fas fa-map-marker-alt"></i>
                            <span>2 Mount ST, Manchester m2 5wq, United Kingdom</span>
                        </li>
                    </ul>
                </div>
                <a href="{{Route('home')}}" class="sub-logo center-align"><img src="{{asset('frontend/images/logo.png')}}" alt=""></a>
                <div class="fr">
                    <ul class="list-style-2 ul-list color-type-1">
                        <li>
                            <i class="fas fa-phone-volume"></i>
                            <a href="tel:" class="link-hover"> +44 161 819 1187</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    
        <div class="main-content">
    
            {{--  <!-- ========< Carousel Section Starts >=============== -->  --}}
            <section class="full-height-slider arrow-closest mobile-pagination">
                <div class="swiper-container full-h" data-mode="horizontal" data-autoplay="0" data-effect="slide" data-slides-per-view="1" data-loop="10" data-speed="1000" data-parallax="1">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="bg layer" style="background-image: url( {{ asset('frontend/images/home-2/home2_slider_img_1.jpg') }} )"></div>
                            <div class="table-view mobile-rel-block">
                                <div class="table-cell">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-10 col-md-offset-1">
                                                <div class="main-caption text-center color-type-1">
                                                    <div class="title h1" data-swiper-parallax="-300" data-swiper-parallax-duration="1000">Explore The Heaven Of Taste</div>
                                                    <div class="empty-sm-20 empty-xs-20"></div>
                                                    <img src="{{asset('frontend/images/title_sepp_2.png')}}" alt="" class="color-overlay" data-swiper-parallax="-400" data-swiper-parallax-duration="1300">
                                                    <div class="empty-sm-25 empty-xs-25"></div>
                                                    <div class="row">
                                                        <div class="col-md-8 col-md-offset-2">
                                                            <div class="simple-text md" data-swiper-parallax="-550" data-swiper-parallax-duration="1500">
                                                                <p><b>We combine all the techniques from Indian cuisines and add magical spices to give you a  titillating food experience.</b></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="empty-sm-50 empty-xs-35"></div>
                                                    <div class="button-wrap" data-swiper-parallax="-900" data-swiper-parallax-duration="1600">
                                                        <a href="{{asset('frontend/files/food_menu.pdf')}}" target="_blank" class="page-button button-style-3 type-2"><span class="txt">Our Food Menu</span><i></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="bg layer" style="background-image: url( {{ asset('frontend/images/home-2/home2_slider_img_2.jpg') }} )"></div>
                            <div class="table-view mobile-rel-block">
                                <div class="table-cell">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-10 col-md-offset-1">
                                                <div class="main-caption text-center color-type-1">
                                                    <div class="title h1" data-swiper-parallax="-300" data-swiper-parallax-duration="1000">Classic experience of Indian dishes</div>
                                                    <div class="empty-sm-20 empty-xs-20"></div>
                                                    <img src="{{asset('frontend/images/title_sepp_2.png')}}" alt="" class="color-overlay" data-swiper-parallax="-400" data-swiper-parallax-duration="1300">
                                                    <div class="empty-sm-25 empty-xs-25"></div>
                                                    <div class="row">
                                                        <div class="col-md-8 col-md-offset-2">
                                                            <div class="simple-text md" data-swiper-parallax="-550" data-swiper-parallax-duration="1500">
                                                                <p><b>Our dining room provides both glamour and comfort, perfect for family outings.</b></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="empty-sm-50 empty-xs-35"></div>
                                                    <div class="button-wrap" data-swiper-parallax="-900" data-swiper-parallax-duration="1600">
                                                        <a href="{{route('gallery')}}" class="page-button button-style-3 type-2"><span class="txt">Food Gallery</span><i></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="bg layer" style="background-image: url( {{ asset('frontend/images/home-2/home2_slider_img_3.jpg') }} )"></div>
                            <div class="table-view mobile-rel-block">
                                <div class="table-cell">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-10 col-md-offset-1">
                                                <div class="main-caption text-center color-type-1">
                                                    <div class="title h1" data-swiper-parallax="-300" data-swiper-parallax-duration="1000">Eat Good you Feel Good</div>
                                                    <div class="empty-sm-20 empty-xs-20"></div>
                                                    <img src="{{asset('frontend/images/title_sepp_2.png')}}" alt="" class="color-overlay" data-swiper-parallax="-400" data-swiper-parallax-duration="1300">
                                                    <div class="empty-sm-25 empty-xs-25"></div>
                                                    <div class="row">
                                                        <div class="col-md-8 col-md-offset-2">
                                                            <div class="simple-text md" data-swiper-parallax="-550" data-swiper-parallax-duration="1500">
                                                                <p><b>It’s not a diet, it’s called eating healthy</b></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="empty-sm-50 empty-xs-35"></div>
                                                    <div class="button-wrap" data-swiper-parallax="-900" data-swiper-parallax-duration="1600">
                                                        <a href="{{Route('reservation')}}" class="page-button button-style-3 type-2"><span class="txt">Book a Table</span><i></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pagination swiper-pagination-2"></div>
                </div>
                <div class="swiper-arrow-left swiper-arrow">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0 0 129 129" enable-background="new 0 0 129 129" width="19px" height="19px">
                        <g>
                            <path d="m88.6,121.3c0.8,0.8 1.8,1.2 2.9,1.2s2.1-0.4 2.9-1.2c1.6-1.6 1.6-4.2 0-5.8l-51-51 51-51c1.6-1.6 1.6-4.2 0-5.8s-4.2-1.6-5.8,0l-54,53.9c-1.6,1.6-1.6,4.2 0,5.8l54,53.9z" fill="#FFFFFF" />
                        </g>
                    </svg>
                </div>
                <div class="swiper-arrow-right swiper-arrow">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0 0 129 129" enable-background="new 0 0 129 129" width="19px" height="19px">
                        <g>
                            <path d="m40.4,121.3c-0.8,0.8-1.8,1.2-2.9,1.2s-2.1-0.4-2.9-1.2c-1.6-1.6-1.6-4.2 0-5.8l51-51-51-51c-1.6-1.6-1.6-4.2 0-5.8 1.6-1.6 4.2-1.6 5.8,0l53.9,53.9c1.6,1.6 1.6,4.2 0,5.8l-53.9,53.9z" fill="#FFFFFF" />
                        </g>
                    </svg>
                </div>
            </section>
            {{--  <!-- ========< Carousel Section Ends >=============== -->  --}}
    
    
            {{--  <!-- ========< About Section Starts >=============== -->  --}}
            <section class="section about-section">
                <div class="bg auto" style="background-image: url({{asset('frontend/images/home-2/section_home2_bg_1.jpg')}})"></div>
                <div class="empty-lg-100 empty-md-60 empty-sm-0 empty-xs-0"></div>
                <div class="custome-container">
                    <div class="row item-animation scroll-type-1">
                        <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12">
                            <div class="simple-item text-center color-type-2 simple-item-style-1">
                                <div class="wrap-padding">
                                    <h2 class="h2 title main-col first-div">About us</h2>
                                    <img src="{{asset('frontend/images/title_sepp_2.png')}}" alt="" class="color-overlay">
                                    <div class="empty-sm-35  empty-xs-35"></div>
                                    <div class="simple-text md">
                                        <p>
                                        Our delicious food is always consistently the best, and the service bends over backwards to make your experience truly memorable.
                                        </p>
                                    </div>
                                </div>
                                <div class="empty-sm-50  empty-xs-50"></div>
                                <a href="about.php" class="page-button button-style-3"><span class="txt">Read More</span><i></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="empty-lg-160 empty-md-100 empty-sm-60 empty-xs-60"></div> -->
                <div class="empty-lg-100 empty-md-60 empty-sm-0 empty-xs-0"></div>
            </section>
            {{--  <!-- ========< About Section Ends >=============== -->  --}}
    
    
            {{--  <!-- ========< Gallary Section Starts >=============== -->  --}}
            <section class="section gallary-section">
                <div class="empty-lg-120 empty-md-90 empty-sm-60 empty-xs-60"></div>
                <div class="container">
                    <div class="row item-animation scroll-type-1">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="main-caption text-center">
                                <h2 class="h2 title main-col">Gallery</h2>
                                <div class="empty-sm-5 empty-xs-5"></div>
                                <img src="{{asset('frontend/images/title_sepp_2.png')}}" alt="" class="color-overlay">
                                <div class="empty-lg-70 empty-md-60 empty-sm-60 empty-xs-40"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid no-padd no-padd-sm">
                    <div class="row item-animation scroll-type-1">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="menu-item menu-item-2 color-type-1">
                                <div class="image hover-zoom">
                                    <img src="{{ asset('frontend/images/home-2/gallery_image_1.jpg') }}" alt="">
                                    <div class="vertical-align full menu-button">
                                        <h5 class="h5 caption"><a href="{{route('gallery')}}" data-open="popup-gallery"><i class="fas fa-plus fa-3x"></i></a></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="menu-item menu-item-2 color-type-1">
                                <div class="image hover-zoom">
                                    <img src="{{ asset('frontend/images/home-2/gallery_image_2.jpg') }}" alt="">
                                    <div class="vertical-align full menu-button">
                                        <h5 class="h5 caption"><a href="{{route('gallery')}}" data-open="popup-gallery"><i class="fas fa-plus fa-3x"></i></a></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="menu-item menu-item-2 color-type-1">
                                <div class="image hover-zoom">
                                    <img src="{{ asset('frontend/images/home-2/gallery_image_3.jpg') }}" alt="">
                                    <div class="vertical-align full menu-button">
                                        <h5 class="h5 caption"><a href="{{route('gallery')}}" data-open="popup-gallery"><i class="fas fa-plus fa-3x"></i></a></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="menu-item menu-item-2 color-type-1">
                                <div class="image hover-zoom">
                                    <img src="{{ asset('frontend/images/home-2/gallery_image_4.jpg') }}" alt="">
                                    <div class="vertical-align full menu-button">
                                        <h5 class="h5 caption"><a href="{{route('gallery')}}" data-open="popup-gallery"><i class="fas fa-plus fa-3x"></i></a></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="menu-item menu-item-2 color-type-1">
                                <div class="image hover-zoom">
                                    <img src="{{ asset('frontend/images/home-2/gallery_image_5.jpg') }}" alt="">
                                    <div class="vertical-align full menu-button">
                                        <h5 class="h5 caption"><a href="{{route('gallery')}}" data-open="popup-gallery"><i class="fas fa-plus fa-3x"></i></a></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="menu-item menu-item-2 color-type-1">
                                <div class="image hover-zoom">
                                    <img src="{{ asset('frontend/images/home-2/gallery_image_6.jpg') }}" alt="">
                                    <div class="vertical-align full menu-button">
                                        <h5 class="h5 caption"><a href="{{route('gallery')}}" data-open="popup-gallery"><i class="fas fa-plus fa-3x"></i></a></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="menu-item menu-item-2 color-type-1">
                                <div class="image hover-zoom">
                                    <img src="{{ asset('frontend/images/home-2/gallery_image_7.jpg') }}" alt="">
                                    <div class="vertical-align full menu-button">
                                        <h5 class="h5 caption"><a href="{{route('gallery')}}" data-open="popup-gallery"><i class="fas fa-plus fa-3x"></i></a></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="menu-item menu-item-2 color-type-1">
                                <div class="image hover-zoom">
                                    <img src="{{ asset('frontend/images/home-2/gallery_image_8.jpg') }}" alt="">
                                    <div class="vertical-align full menu-button">
                                        <h5 class="h5 caption"><a href="{{route('gallery')}}" data-open="popup-gallery"><i class="fas fa-plus fa-3x"></i></a></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            {{--  <!-- ========< Gallary Section Ends >=============== -->  --}}
    
    
            {{--  <!-- ========< Contact Section Starts >=============== -->  --}}
            <section class="section">
                <div class="bg left contain md-hide" style="background-image: url( {{asset('frontend/images/home-2/contact_left_img.jpg')}} )"></div>
                <div class="bg right contain md-hide" style="background-image: url( {{asset('frontend/images/home-2/contact_right_img.jpg')}} )"></div>
                <div class="empty-lg-120 empty-md-90 empty-sm-60 empty-xs-60"></div>
                <div class="wrap">
                    <div class="container">
                        <div class="row item-animation scroll-type-1">
                            <div class="col-md-6 col-md-offset-3">
                                <div class="main-caption text-center">
                                    <h2 class="h2 title main-col">Contact</h2>
                                    <div class="empty-sm-5 empty-xs-5"></div>
                                    <img src="{{asset('frontend/images/title_sepp_2.png')}}" alt="" class="color-overlay">
                                    <div class="empty-lg-30 empty-md-20 empty-sm-20 empty-xs-10"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row item-animation scroll-type-1">
                            <div class="col-md-8 col-md-offset-2">
                                <ul class="list-style-2 ul-list text-center">
                                    <li>
                                        <span><b>Address:</b> 2 Mount ST,
                                            Manchester M2 5WQ, 
                                            United Kingdom</span>
                                    </li>
                                    <li>
                                        <a href="tel:+44 161 819 1187" class="link-hover"><b>Phone:</b>+44 161 819 1187</a>
                                    </li>
                                </ul>
                                <div class="empty-sm-50 empty-xs-50"></div>

                                <form method="POST" action="{{ route('contact.store')}}" id="messageForm3">
                                @csrf
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="input-field-wrap">
                                                <input type="text" class="input-field" placeholder="First Name *" name="first_name" required>
                                                <div class="focus"></div>
                                            </div>
                                            <div class="empty-sm-20 empty-xs-20"></div>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="input-field-wrap">
                                                <input type="text" class="input-field" placeholder="Last Name *" name="last_name" required>
                                                <div class="focus"></div>
                                            </div>
                                            <div class="empty-sm-20 empty-xs-20"></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="input-field-wrap">
                                                <input type="email" class="input-field" placeholder="Email *" name="email" required>
                                                <div class="focus"></div>
                                            </div>
                                            <div class="empty-sm-20 empty-xs-20"></div>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="input-field-wrap">
                                                <input type="text" class="input-field" placeholder="Subject" name="subject">
                                                <div class="focus"></div>
                                            </div>
                                            <div class="empty-sm-20 empty-xs-20"></div>
                                        </div>
                                    </div>
                                    <div class="input-field-wrap">
                                        <textarea placeholder="Message *" class="input-field" name="message"></textarea>
                                        <div class="focus"></div>
                                    </div>
                                    <div class="empty-sm-60 empty-xs-60"></div>
                                    <div class="text-center">
                                        <div class="page-button button-style-3">
                                            <input type="submit">
                                            <span class="txt">send a message</span><i></i>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="empty-lg-160 empty-md-100 empty-sm-60 empty-xs-60"></div>
    
            </section>
            {{--  <!-- ========< Contact Section Ends >=============== -->  --}}
    
    
            {{--  <!-- ========< Visit-Us Section Starts >=============== -->  --}}
            <section class="section">
                <div class="bg fix layer" style="background-image: url( {{asset('frontend/images/home-2/section_home2_bg_4.jpg')}} )"></div>
                <div class="empty-lg-140 empty-md-100 empty-sm-60 empty-xs-60"></div>
                <div class="container no-padd">
                    <div class="row vertical-wrap item-animation scroll-type-1">
                        <div class="col-md-7 col-sm-12 col-xs-12">
                            <div class="simple-item-style-2 simple-item color-type-2 text-center min-h-450">
                                <div class="wrap-padding">
                                    <div class="main-title">
                                        <h2 class="h2 title main-col">Our Food Menu</h2>
                                        <div class="empty-sm-5 empty-xs-5"></div>
                                        <img src="{{asset('frontend/images/title_sepp_2.png')}}" alt="" class="color-overlay">
                                        <div class="empty-sm-80  empty-xs-50"></div>
                                        <a href="{{asset('frontend/files/food_menu.pdf')}}" target="_blank" class="page-button button-style-3" target="_blank"><span class="txt">Download Our Food Menu</span><i></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="empty-lg-0 empty-md-0 empty-sm-15 empty-xs-15"></div>
                        </div>
                        <div class="col-md-5 col-sm-12 col-xs-12">
                            <div class="simple-item-style-2 simple-item color-type-1 text-center min-h-450">
                                <div class="wrap-padding">
                                    <div class="empty-lg-20 empty-md-0 empty-sm-0 empty-xs-0"></div>
                                    <h4 class="h4 caption">Opening Hours</h4>
                                    <div class="empty-sm-30 empty-xs-30"></div>
                                    <ul class="list-style-1 ul-list">
                                        <li>
                                            <div class="flex-wrap"><span>Tuesday</span><i></i><b>04.30 pm - 10.30 pm</b></div>
                                        </li>
                                        <li>
                                            <div class="flex-wrap"><span>Wednesday</span><i></i><b>04.30 pm - 10.30 pm</b></div>
                                        </li>
                                        <li>
                                            <div class="flex-wrap"><span>Thrusday</span><i></i><b>04.30 pm - 10.30 pm</b></div>
                                        </li>
                                        <li>
                                            <div class="flex-wrap"><span>Friday</span><i></i><b>04.30 pm - 11.30 pm</b></div>
                                        </li>
                                        <li>
                                            <div class="flex-wrap"><span>Saturday</span><i></i><b>04.30 pm - 11.30 pm</b></div>
                                        </li>
                                        <li>
                                            <div class="flex-wrap"><span>Sunday</span><i></i><b>03.30 pm - 09.00 pm</b></div>
                                        </li>
                                        <li>
                                            <div class="flex-wrap"><span>Monday</span><i></i><b>Closed</b></div>
                                        </li>
                                    </ul>
                                    <div class="empty-lg-20 empty-md-0 empty-sm-0 empty-xs-0"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="empty-lg-140 empty-md-100 empty-sm-60 empty-xs-60"></div>
            </section>
            {{--  <!-- ========< Visit-Us Section Ends >=============== -->  --}}
        </div>
    
    @include('partials.frontend.footer')
    @include('partials.frontend.script')
    {{--  This will include all JS files which are connected into javascript.blade.php in partials Folder  --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script>
        // Inline Javascript Here    
        window.onload = function() {
            $('.header-menu .contact').addClass('active');
        };
    </script>
</body>
</html>