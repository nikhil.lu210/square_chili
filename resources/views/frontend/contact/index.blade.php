@extends('layouts.frontend')

@section('page_title', '| CONTACT')

@section('stylesheet')
    {{--  External CSS  --}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet" />
    <style>
    /* Inline CSS Here */
    .alert {
        position: relative;
        padding: .75rem 1.25rem;
        margin-bottom: 1rem;
        border: 1px solid transparent;
        border-radius: .25rem
    }
    
    .alert-heading {
        color: inherit
    }
    
    .alert-link {
        font-weight: 700
    }
    
    .alert-dismissible {
        padding-right: 4rem
    }
    
    .alert-dismissible .close {
        position: absolute;
        top: 0;
        right: 0;
        padding: .75rem 1.25rem;
        color: inherit
    }
    
    .alert-primary {
        color: #004085;
        background-color: #cce5ff;
        border-color: #b8daff
    }
    
    .alert-primary hr {
        border-top-color: #9fcdff
    }
    
    .alert-primary .alert-link {
        color: #002752
    }
    
    .alert-secondary {
        color: #383d41;
        background-color: #e2e3e5;
        border-color: #d6d8db
    }
    
    .alert-secondary hr {
        border-top-color: #c8cbcf
    }
    
    .alert-secondary .alert-link {
        color: #202326
    }
    
    .alert-success {
        color: #155724;
        background-color: #d4edda;
        border-color: #c3e6cb
    }
    
    .alert-success hr {
        border-top-color: #b1dfbb
    }
    
    .alert-success .alert-link {
        color: #0b2e13
    }
    
    .alert-info {
        color: #0c5460;
        background-color: #d1ecf1;
        border-color: #bee5eb
    }
    
    .alert-info hr {
        border-top-color: #abdde5
    }
    
    .alert-info .alert-link {
        color: #062c33
    }
    
    .alert-warning {
        color: #856404;
        background-color: #fff3cd;
        border-color: #ffeeba
    }
    
    .alert-warning hr {
        border-top-color: #ffe8a1
    }
    
    .alert-warning .alert-link {
        color: #533f03
    }
    
    .alert-danger {
        color: #721c24;
        background-color: #f8d7da;
        border-color: #f5c6cb
    }
    
    .alert-danger hr {
        border-top-color: #f1b0b7
    }
    
    .alert-danger .alert-link {
        color: #491217
    }
    
    .alert-light {
        color: #818182;
        background-color: #fefefe;
        border-color: #fdfdfe
    }
    
    .alert-light hr {
        border-top-color: #ececf6
    }
    
    .alert-light .alert-link {
        color: #686868
    }
    
    .alert-dark {
        color: #1b1e21;
        background-color: #d6d8d9;
        border-color: #c6c8ca
    }
    
    .alert-dark hr {
        border-top-color: #b9bbbe
    }
    
    .alert-dark .alert-link {
        color: #040505
    }
    </style>
@endsection

@section('content')
    {{--  Body Contents Here  --}}
    <div class="main-content">
        <section class="full-height-slider type-1">
            <div class="bg ready data-jarallax" data-jarallax="5" style="background-image: url({{asset('frontend/images/contacts/main-baner-bg.jpg')}})" data-swiper-parallax="50%"></div>
            <div class="table-view mobile-rel-block">
                <div class="table-cell">
                    <div class="container no-padd">
                        <div class="row vertical-wrap">
                            <div class="col-lg-6 col-lg-offset-3 col-sm-12">
                                <div class="simple-item color-type-1 text-center">
                                    <div class="main-title">
                                        <h1 class="h1 caption">Our Contacts</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="location-bottom type-2">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-4 col-xs-12">
                                    <div class="empty-sm-70 empty-xs-30"></div>
                                    <div class="text-center color-type-3">
                                        <div class="contact-icon">
                                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512.192 512.192" width="65px" height="65px" style="enable-background:new 0 0 512.192 512.192;" xml:space="preserve">
                                                <g>
                                                    <g>
                                                        <g>
                                                            <path d="M440.805,386.456l-60.907-47.467c-17.173-13.44-41.173-13.333-58.24,0.213l-13.547,10.667
                        c-10.24,8.107-24.96,7.253-34.24-2.027l-108.907-109.12c-9.28-9.28-10.133-24.107-2.027-34.453l10.667-13.547
                        c13.547-17.067,13.547-41.173,0.213-58.24l-47.36-61.013c-8.32-10.667-20.8-17.28-34.347-18.133
                        c-13.44-0.747-26.667,4.267-36.267,13.76l-38.827,38.933c-21.653,21.653-52.907,90.987,122.773,266.987
                        c114.453,114.773,184.32,138.88,222.72,138.88c23.147,0,36.587-8.533,43.733-15.787l38.933-38.933
                        c18.453-18.453,18.347-48.427-0.107-66.88C443.685,388.909,442.298,387.629,440.805,386.456z M432.165,440.216
                        c-0.747,0.853-1.387,1.6-2.133,2.347l-38.933,38.933c-4.267,4.373-12.8,9.493-28.693,9.493
                        c-28.16,0-92.587-17.173-207.68-132.587c-132.48-132.8-148.907-210.56-122.667-236.8l38.933-39.04
                        c4.8-4.8,11.307-7.573,18.24-7.573c0.533,0,1.067,0,1.6,0c7.36,0.427,14.187,4.053,18.773,9.92l47.36,61.013
                        c7.36,9.387,7.253,22.613-0.107,32l-10.667,13.547c-14.827,18.773-13.227,45.653,3.627,62.72l108.907,109.227
                        c16.96,16.96,43.84,18.56,62.613,3.627l13.547-10.667c9.28-7.36,22.507-7.467,31.893-0.107l60.907,47.467
                        C438.992,412.589,441.018,428.909,432.165,440.216z"></path>
                                                            <path d="M509.072,3.416c-4.16-4.16-10.88-4.16-15.04,0L288.165,209.282v-91.2c0-5.333-3.84-10.133-9.067-10.88
                        c-6.613-0.96-12.267,4.16-12.267,10.56v117.333c0,5.867,4.8,10.667,10.667,10.667h106.347c5.333,0,10.133-3.84,10.88-9.067
                        c0.96-6.613-4.16-12.267-10.56-12.267h-80.96L509.072,18.562C513.232,14.402,513.232,7.576,509.072,3.416z"></path>
                                                        </g>
                                                    </g>
                                                </g>
                                                <g></g>
                                                <g></g>
                                                <g></g>
                                                <g></g>
                                                <g></g>
                                                <g></g>
                                                <g></g>
                                                <g></g>
                                                <g></g>
                                                <g></g>
                                                <g></g>
                                                <g></g>
                                                <g></g>
                                                <g></g>
                                                <g></g>
                                            </svg>
                                        </div>
                                        <div class="empty-sm-15 empty-xs-10"></div>
                                        <h4 class="h4 tt color-type-3">Call us</h4>
                                        <div class="empty-sm-20 empty-xs-10"></div>
                                        <div class="simple-text contact color-2">
                                            <a href="tel:+44 161 819 1187" class="link-hover">+44 161 819 1187</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <div class="empty-sm-70 empty-xs-30"></div>
                                    <div class="text-center color-type-3">
                                        <div class="contact-icon">
                                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="65px" height="65px" viewBox="0 0 491.582 491.582" style="enable-background:new 0 0 491.582 491.582;" xml:space="preserve">
                                                <g>
                                                    <g>
                                                        <path d="M245.791,0C153.799,0,78.957,74.841,78.957,166.833c0,36.967,21.764,93.187,68.493,176.926
                        c31.887,57.138,63.627,105.4,64.966,107.433l22.941,34.773c2.313,3.507,6.232,5.617,10.434,5.617s8.121-2.11,10.434-5.617
                        l22.94-34.771c1.326-2.01,32.835-49.855,64.967-107.435c46.729-83.735,68.493-139.955,68.493-176.926
                        C412.625,74.841,337.783,0,245.791,0z M322.302,331.576c-31.685,56.775-62.696,103.869-64.003,105.848l-12.508,18.959
                        l-12.504-18.954c-1.314-1.995-32.563-49.511-64.007-105.853c-43.345-77.676-65.323-133.104-65.323-164.743
                        C103.957,88.626,167.583,25,245.791,25s141.834,63.626,141.834,141.833C387.625,198.476,365.647,253.902,322.302,331.576z"></path>
                                                        <path d="M245.791,73.291c-51.005,0-92.5,41.496-92.5,92.5s41.495,92.5,92.5,92.5s92.5-41.496,92.5-92.5
                        S296.796,73.291,245.791,73.291z M245.791,233.291c-37.22,0-67.5-30.28-67.5-67.5s30.28-67.5,67.5-67.5
                        c37.221,0,67.5,30.28,67.5,67.5S283.012,233.291,245.791,233.291z"></path>
                                                    </g>
                                                </g>
                                                <g></g>
                                                <g></g>
                                                <g></g>
                                                <g></g>
                                                <g></g>
                                                <g></g>
                                                <g></g>
                                                <g></g>
                                                <g></g>
                                                <g></g>
                                                <g></g>
                                                <g></g>
                                                <g></g>
                                                <g></g>
                                                <g></g>
                                            </svg>
                                        </div>
                                        <div class="empty-sm-15 empty-xs-10"></div>
                                        <h4 class="h4 tt color-type-3">FIND us</h4>
                                        <div class="empty-sm-20 empty-xs-10"></div>
                                        <div class="simple-text color-2 contact">
                                            <p>2 MOUNT ST, MANCHESTER M2 5WQ, United Kingdom</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <div class="empty-sm-70 empty-xs-30"></div>
                                    <div class="text-center color-type-3">
                                        <div class="contact-icon">
                                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 44 44" width="65px" height="65px" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 44 44">
                                                <g>
                                                    <g>
                                                        <g>
                                                            <path d="M43,6H1C0.447,6,0,6.447,0,7v30c0,0.553,0.447,1,1,1h42c0.552,0,1-0.447,1-1V7C44,6.447,43.552,6,43,6z M42,33.581     L29.612,21.194l-1.414,1.414L41.59,36H2.41l13.392-13.392l-1.414-1.414L2,33.581V8h40V33.581z"></path>
                                                        </g>
                                                    </g>
                                                    <g>
                                                        <g>
                                                            <path d="M39.979,8L22,25.979L4.021,8H2v0.807L21.293,28.1c0.391,0.391,1.023,0.391,1.414,0L42,8.807V8H39.979z"></path>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                                        </div>
                                        <div class="empty-sm-15 empty-xs-10"></div>
                                        <h4 class="h4 tt color-type-3">MAIL us</h4>
                                        <div class="empty-sm-20 empty-xs-10"></div>
                                        <div class="simple-text contact color-2">
                                            <a href="mailto:squarechili.info@mail.com" class="link-hover">squarechili.info@mail.com</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    
        <section class="section">
            <div class="empty-lg-120 empty-md-100 empty-sm-60 empty-xs-60"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-xs-12">
                        <aside>
                            <div class="text-center color-type-2">
                                <h4 class="h4 tt color-type-1">Opening Time</h4>
                                <div class="empty-sm-10 empty-xs-10"></div>
                                <div class="empty-sm-20 empty-xs-10"></div> 
                                <ul class="list-style-1 ul-list">
                                <li>
                                            <div class="flex-wrap"><span>Tuesday</span><i></i><b>04.30 - 10.30 pm</b></div>
                                        </li>
                                        <li>
                                            <div class="flex-wrap"><span>Wednesday</span><i></i><b>04.30 - 10.30 pm</b></div>
                                        </li>
                                        <li>
                                            <div class="flex-wrap"><span>Thrusday</span><i></i><b>04.30 - 10.30 pm</b></div>
                                        </li>
                                        <li>
                                            <div class="flex-wrap"><span>Friday</span><i></i><b>04.30 - 11.30 pm</b></div>
                                        </li>
                                        <li>
                                            <div class="flex-wrap"><span>Saturday</span><i></i><b>04.30 - 11.30 pm</b></div>
                                        </li>
                                        <li>
                                            <div class="flex-wrap"><span>Sunday</span><i></i><b>03.30 - 09.00 pm</b></div>
                                        </li>
                                        <li>
                                            <div class="flex-wrap"><span>Monday</span><i></i><b>Closed</b></div>
                                        </li>
                                </ul>
                            </div>
                        </aside>
                        <aside>
                            <div class="empty-sm-70 empty-xs-30"></div>
                            <div class="text-center color-type-2">
                                <h4 class="h4 tt color-type-1">follow us</h4>
                                <div class="follow follow-us">
                                    <a href="#">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                    <a href="#">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                    <a href="#">
                                        <i class="fab fa-instagram"></i>
                                    </a>
                                    <a href="#">
                                        <i class="fab fa-tripadvisor"></i>
                                    </a>
                                </div>
                            </div>
                        </aside>
                    </div>
                    <div class="col-md-8 col-xs-12">
                        <div class="text-center color-type-2">
                            <h4 class="h4 tt color-type-1">Please, feel free to contact us.</h4>
                        </div>
                        {{--  <div class="empty-sm-45 empty-xs-30"></div>  --}}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="empty-sm-30  empty-xs-20"></div>
                                <div class="flash-message">
                                    @if( Session::has( 'success' ))
                                        <p class="alert alert-success">
                                            {{ Session::get( 'success' ) }}
                                        </p>
                                    @elseif( Session::has( 'warning' ))
                                        <p class="alert alert-danger">
                                            {{ Session::get( 'Message Sent Failed. Please try Again' ) }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <form id="contact" action="{{Route('contact.store')}}" method="POST" >
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="msg"></div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="input-field-wrap">
                                        <input type="text" class="input-field" placeholder="First Name *" name="first_name" required>
                                        <div class="focus"></div>
                                    </div>
                                    <div class="empty-sm-20 empty-xs-20"></div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="input-field-wrap">
                                        <input type="text" class="input-field" placeholder="Last Name" name="last_name">
                                        <div class="focus"></div>
                                    </div>
                                    <div class="empty-sm-20 empty-xs-20"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <div class="input-field-wrap">
                                        <input type="email" class="input-field" placeholder="Email *" name="email" required>
                                        <div class="focus"></div>
                                    </div>
                                    <div class="empty-sm-20 empty-xs-20"></div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="input-field-wrap">
                                        <input type="text" class="input-field" placeholder="Subject" name="subject">
                                        <div class="focus"></div>
                                    </div>
                                    <div class="empty-sm-20 empty-xs-20"></div>
                                </div>
                            </div>
                            <div class="input-field-wrap">
                                <textarea placeholder="Message *" class="input-field" name="message"></textarea>
                                <div class="focus"></div>
                            </div>
                            <div class="empty-sm-30 empty-xs-30"></div>
                            <div class="text-center">
                                <div class="page-button button-style-1 type-2">
                                    <input type="submit" id="submit-btn">
                                    <span class="txt">SEND MESSAGE</span><i></i>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            
            <div class="empty-sm-40 empty-xs-20"></div> 
        </section>
    </div>
    
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script>
        // Inline Javascript Here    
        window.onload = function() {
            $('.header-menu .contact').addClass('active');
        };
    </script>
@endsection