@extends('layouts.frontend')

@section('page_title', '| GALLERY')

@section('stylesheet')
    {{--  External CSS  --}}
    
@endsection

@section('content')
    {{--  Body Contents Here  --}}
    <div class="main-content">
        <section class="full-height-slider type-1">
            <div class="bg ready data-jarallax" data-jarallax="5" style="background-image: url({{ asset('frontend/images/gallery-1/main-baner-bg.jpg') }})" data-swiper-parallax="50%"></div>
            <div class="table-view mobile-rel-block">
                <div class="table-cell">
                    <div class="container no-padd">
                        <div class="row vertical-wrap">
                            <div class="col-md-6 col-md-offset-3 col-sm-12 col-sm-offset-0">
                                <div class="simple-item color-type-1 text-center">
                                    <div class="main-title">
                                        <h1 class="h1 caption">Our Gallery</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    
        <section class="section">
            <div class="empty-lg-115 empty-md-90 empty-sm-60 empty-xs-60"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 col-sm-12 col-sm-offset-0">
                        <div class="simple-item color-type-2 text-center">
                            <div class="main-title">
                                <h2 class="h2">Our Amazing Photos</h2>
                            </div>
                        </div>
                    </div>
                </div>
    
                <div class="tab-continer text-center">
    
                    <div class="empty-lg-100 empty-md-70 empty-sm-50 empty-xs-40"></div>
                    <div class="row">
                        <div class="izotope-container grid-type-1">
                            <div class="grid-sizer"></div>
                            <div class="item pizzas col-md-6 col-sm-6 col-xs-12">
                                <div class="menu-item menu-item-2 color-type-1">
                                    <div class="image hover-zoom">
                                        <img src="{{ asset('frontend/images/gallery-1/photo-1.jpg' )}}" alt="">
                                    </div>
                                </div>
                                <div class="empty-sm-30 empty-xs-30"></div>
                            </div>
                            <div class="item desserts col-md-6 col-sm-6 col-xs-12">
                                <div class="menu-item menu-item-2 color-type-1">
                                    <div class="image hover-zoom">
                                        <img src="{{ asset('frontend/images/gallery-1/photo-2.jpg' )}}" alt="">
                                    </div>
                                </div>
                                <div class="empty-sm-30 empty-xs-30"></div>
                            </div>
                            <div class="item pizzas col-md-3 col-sm-6 col-xs-12">
                                <div class="menu-item menu-item-2 color-type-1">
                                    <div class="image hover-zoom">
                                        <img src="{{ asset('frontend/images/gallery-1/photo-3.jpg' )}}" alt="">
                                    </div>
                                </div>
                                <div class="empty-sm-30 empty-xs-30"></div>
                            </div>
                            <div class="item other col-md-3 col-sm-6 col-xs-12">
                                <div class="menu-item menu-item-2 color-type-1">
                                    <div class="image hover-zoom">
                                        <img src="{{ asset('frontend/images/gallery-1/photo-4.jpg' )}}" alt="">
                                    </div>
                                </div>
                                <div class="empty-sm-30 empty-xs-30"></div>
                            </div>
                            <div class="item salats col-md-3 col-sm-6 col-xs-12">
                                <div class="menu-item menu-item-2 color-type-1">
                                    <div class="image hover-zoom">
                                        <img src="{{ asset('frontend/images/gallery-1/photo-5.jpg' )}}" alt="">
                                    </div>
                                </div>
                                <div class="empty-sm-30 empty-xs-30"></div>
                            </div>
                            <div class="item salats col-md-3 col-sm-6 col-xs-12">
                                <div class="menu-item menu-item-2 color-type-1">
                                    <div class="image hover-zoom">
                                        <img src="{{ asset('frontend/images/gallery-1/photo-6.jpg' )}}" alt="">
                                    </div>
                                </div>
                                <div class="empty-sm-30 empty-xs-30"></div>
                            </div>
                            <div class="item other col-md-3 col-sm-6 col-xs-12">
                                <div class="menu-item menu-item-2 color-type-1">
                                    <div class="image hover-zoom">
                                        <img src="{{ asset('frontend/images/gallery-1/photo-7.jpg' )}}" alt="">
                                    </div>
                                </div>
                                <div class="empty-sm-30 empty-xs-30"></div>
                            </div>
                            <div class="item process col-md-6 col-sm-6 col-xs-12">
                                <div class="menu-item menu-item-2 color-type-1">
                                    <div class="image hover-zoom">
                                        <img src="{{ asset('frontend/images/gallery-1/photo-8.jpg' )}}" alt="">
                                    </div>
                                </div>
                                <div class="empty-sm-30 empty-xs-30"></div>
                            </div>
                            <div class="item process col-md-6 col-sm-6 col-xs-12">
                                <div class="menu-item salats menu-item-2 color-type-1">
                                    <div class="image hover-zoom">
                                        <img src="{{ asset('frontend/images/gallery-1/photo-11.jpg' )}}" alt="">
                                    </div>
                                </div>
                                <div class="empty-sm-30 empty-xs-30"></div>
                            </div>
                            <div class="item salats col-md-3 col-sm-6 col-xs-12">
                                <div class="menu-item menu-item-2 color-type-1">
                                    <div class="image hover-zoom">
                                        <img src="{{ asset('frontend/images/gallery-1/photo-9.jpg' )}}" alt="">
                                    </div>
                                </div>
                                <div class="empty-sm-30 empty-xs-30"></div>
                            </div>
                            <div class="item process col-md-3 col-sm-6 col-xs-12">
                                <div class="menu-item menu-item-2 color-type-1">
                                    <div class="image hover-zoom">
                                        <img src="{{ asset('frontend/images/gallery-1/photo-10.jpg' )}}" alt="">
                                    </div>
                                </div>
                                <div class="empty-sm-30 empty-xs-30"></div>
                            </div>
                            <div class="item pizzas col-md-3 col-sm-6 col-xs-12">
                                <div class="menu-item menu-item-2 color-type-1">
                                    <div class="image hover-zoom">
                                        <img src="{{ asset('frontend/images/gallery-1/photo-12.jpg' )}}" alt="">
                                    </div>
                                </div>
                                <div class="empty-sm-30 empty-xs-30"></div>
                            </div>
                            <div class="item process col-md-3 col-sm-6 col-xs-12">
                                <div class="menu-item menu-item-2 color-type-1">
                                    <div class="image hover-zoom">
                                        <img src="{{ asset('frontend/images/gallery-1/photo-13.jpg' )}}" alt="">
                                    </div>
                                </div>
                                <div class="empty-sm-30 empty-xs-30"></div>
                            </div>
                            <div class="item other col-md-3 col-sm-6 col-xs-12">
                                <div class="menu-item menu-item-2 color-type-1">
                                    <div class="image hover-zoom">
                                        <img src="{{ asset('frontend/images/gallery-1/photo-14.jpg' )}}" alt="">
                                    </div>
                                </div>
                                <div class="empty-sm-30 empty-xs-30"></div>
                            </div>
                            <div class="item desserts col-md-3 col-sm-6 col-xs-12">
                                <div class="menu-item menu-item-2 color-type-1">
                                    <div class="image hover-zoom">
                                        <img src="{{ asset('frontend/images/gallery-1/photo-15.jpg' )}}" alt="">
                                    </div>
                                </div>
                                <div class="empty-sm-30 empty-xs-30"></div>
                            </div>
                        </div>
                    </div>
                    <div class="empty-lg-70 empty-md-40 empty-sm-20 empty-xs-10"></div>
                </div>
            </div>
        </section>
    
    </div>
    
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.1/isotope.pkgd.min.js"></script>
    <script>
        // Inline Javascript Here
        window.onload = function() {
            $('.header-menu .gallery').addClass('active');
        };
    </script>
@endsection