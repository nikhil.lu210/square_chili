@extends('layouts.frontend')

@section('page_title', '| ABOUT')

@section('stylesheet')
    {{--  External CSS  --}}
    
@endsection

@section('content')
    {{--  Body Contents Here  --}}
    <div class="main-content">
        <section class="full-height-slider type-1">
            <div class="bg ready data-jarallax" data-jarallax="5" style="background-image: url({{ asset('frontend/images/about/main-baner-bg.jpg') }})" data-swiper-parallax="50%"></div>
            <div class="table-view mobile-rel-block">
                <div class="table-cell">
                    <div class="container no-padd">
                        <div class="row vertical-wrap">
                            <div class="col-md-6 col-md-offset-3 col-sm-12 col-sm-offset-0">
                                <div class="simple-item color-type-1 text-center">
                                    <div class="main-title">
                                        <h1 class="h1 caption">About Us</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    
        <section class="section about-section">
            <div class="empty-lg-60 empty-md-60 empty-sm-60 empty-xs-60"></div>
            <div class="container">
                <div class="row left-right-item">
                    <div class="col-md-6">
                        <div class="simple-item-style-2 simple-item text-center">
                            <div class="line-sepp"></div>
                            <div class="empty-sm-10"></div>
                            <h2 class="h2">About Square Chili</h2>
                            <div class="empty-sm-20 empty-xs-20"></div>
                            <div class="box-padd">
                                <div class="simple-text">
                                    <p>Square chili Restaurant is located on 2 Mount ST, Manchester M2 5WQ in UK. We serve the finest Indian Cuisine that will surely give everyone an amazing food experience.</p>
                                </div>
                                <div class="empty-sm-25 empty-xs-20"></div>
                                <div class="simple-text">
                                    <p>
                                        Our delicious food is always consistently the best, and the service bends over backwards to make your experience truly memorable. Our experienced Chef uses fresh and special ingredients to prepare food, made with a lot of passion and desire to serve our lovely customers.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="empty-md-0 empty-sm-30 empty-xs-30"></div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-6">
    
                                <div class="row">
                                    <div class="col-sm-12">
                                        <img src="{{ asset('frontend/images/about/recognition-1.jpg') }}" alt="" class="full-img">
                                    </div>
                                    <div class="empty-sm-30 empty-xs-30"></div>
                                    <div class="col-sm-12">
                                        <div class="wrapp-border">
                                            <span class="page-span-5 page-span left"></span>
                                            <img src="{{ asset('frontend/images/about/recognition-2.jpg') }}" alt="" class="full-img">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="wrapp-border">
                                    <span class="page-span-6 page-span right"></span>
                                    <img src="{{ asset('frontend/images/about/recognition-3.jpg') }}" alt="" class="full-img">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="empty-lg-60 empty-md-60 empty-sm-60 empty-xs-60"></div>
        </section>
    </div>
    
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script>
        // Inline Javascript Here
        window.onload = function() {
            $('.header-menu .about').addClass('active');
        };
    </script>
@endsection