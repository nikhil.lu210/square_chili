@extends('layouts.frontend')

@section('page_title', '| RESERVATION')

@section('stylesheet')
    {{--  External CSS  --}}
    <link href="{{asset('frontend/css/nice-select.min.css')}}" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet" />
    <style>
        /* Inline CSS Here */
        input[type=date]::-webkit-inner-spin-button {
            -webkit-appearance: none;
            display: none;
        }
        input[type=time]::-webkit-inner-spin-button {
            -webkit-appearance: none;
            display: none;
        }
        .nice-select{
            border: 1px solid rgba(209, 209, 209, 0.7);
            border-radius: 0px;
            height: 44px;
            width: 100%;
        }
        .nice-select.open, .nice-select:active, .nice-select:focus{
            border-color: #eb3b2c;
        }
        .nice-select.open ul.list{
            width: 100%;
            max-height: 200px;
            overflow-y: scroll;
        }
        label{
            float: left;
            color: #666;
            font-size: 16px;
            padding-bottom: 5px;
            padding-top: 5px;
        }

        .alert {
        position: relative;
        padding: .75rem 1.25rem;
        margin-bottom: 1rem;
        border: 1px solid transparent;
        border-radius: .25rem
    }

    .alert-heading {
        color: inherit
    }

    .alert-link {
        font-weight: 700
    }

    .alert-dismissible {
        padding-right: 4rem
    }

    .alert-dismissible .close {
        position: absolute;
        top: 0;
        right: 0;
        padding: .75rem 1.25rem;
        color: inherit
    }

    .alert-primary {
        color: #004085;
        background-color: #cce5ff;
        border-color: #b8daff
    }

    .alert-primary hr {
        border-top-color: #9fcdff
    }

    .alert-primary .alert-link {
        color: #002752
    }

    .alert-secondary {
        color: #383d41;
        background-color: #e2e3e5;
        border-color: #d6d8db
    }

    .alert-secondary hr {
        border-top-color: #c8cbcf
    }

    .alert-secondary .alert-link {
        color: #202326
    }

    .alert-success {
        color: #155724;
        background-color: #d4edda;
        border-color: #c3e6cb
    }

    .alert-success hr {
        border-top-color: #b1dfbb
    }

    .alert-success .alert-link {
        color: #0b2e13
    }

    .alert-info {
        color: #0c5460;
        background-color: #d1ecf1;
        border-color: #bee5eb
    }

    .alert-info hr {
        border-top-color: #abdde5
    }

    .alert-info .alert-link {
        color: #062c33
    }

    .alert-warning {
        color: #856404;
        background-color: #fff3cd;
        border-color: #ffeeba
    }

    .alert-warning hr {
        border-top-color: #ffe8a1
    }

    .alert-warning .alert-link {
        color: #533f03
    }

    .alert-danger {
        color: #721c24;
        background-color: #f8d7da;
        border-color: #f5c6cb
    }

    .alert-danger hr {
        border-top-color: #f1b0b7
    }

    .alert-danger .alert-link {
        color: #491217
    }

    .alert-light {
        color: #818182;
        background-color: #fefefe;
        border-color: #fdfdfe
    }

    .alert-light hr {
        border-top-color: #ececf6
    }

    .alert-light .alert-link {
        color: #686868
    }

    .alert-dark {
        color: #1b1e21;
        background-color: #d6d8d9;
        border-color: #c6c8ca
    }

    .alert-dark hr {
        border-top-color: #b9bbbe
    }

    .alert-dark .alert-link {
        color: #040505
    }
    </style>
@endsection

@section('content')
    {{--  Body Contents Here  --}}
    <div class="main-content">
        <section class="full-height-slider type-1">
            <div class="bg ready data-jarallax" data-jarallax="5" style="background-image: url( {{ asset('frontend/images/reservation/main-baner-bg.jpg') }} )" data-swiper-parallax="50%"></div>
            <div class="table-view mobile-rel-block">
                <div class="table-cell">
                    <div class="container no-padd">
                        <div class="row vertical-wrap">
                            <div class="col-md-6 col-md-offset-3 col-sm-12 col-sm-offset-0">
                                <div class="simple-item color-type-1 text-center">
                                    <div class="main-title">
                                        <h1 class="h1 caption">Reservation</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    
        <section class="section reservation-section">
            <div class="empty-lg-115 empty-md-100 empty-sm-60 empty-xs-60"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-xs-12">
                        <div class="main-caption text-center color-type-2">
                            <h2 class="h2">Book A Table</h2>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="empty-sm-30  empty-xs-20"></div>
                                    <div class="flash-message">
                                        @if( Session::has( 'success' ))
                                            <p class="alert alert-success">
                                                {{ Session::get( 'success' ) }}
                                            </p>
                                        @elseif( Session::has( 'warning' ))
                                            <p class="alert alert-danger">
                                                {{ Session::get( 'Reservation Failed. Please try Again' ) }}
                                            </p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            
                            <form id="contact" action="{{Route('reservation.store')}}" method="POST">
                                @csrf
                                <div class="col-md-12">
                                    <div id="msg"></div>
                                </div>
                                <div class="col-md-12">
                                    <label for="ful_name">Full Name</label>
                                    <input type="text" class="input-field" placeholder="Jhon Doe." name="full_name" required="">
                                    <div class="empty-sm-20  empty-xs-20"></div>
                                </div>
                                
                                <div class="col-md-5" style="padding-bottom: 20px;">
                                    <label for="date">Date</label>
                                    <input type="date" class="input-field" placeholder="DD / MM / YY" name="date" required="">
                                </div>
                                <div class="col-md-3">
                                    <label for="time">Time</label>
                                    <select name="time" class="input-field">
                                        <option class="text-muted">Pick Time</option>
                                        <option value="03.30pm">03.30 pm</option>
                                        <option value="03.45pm">03.45 pm</option>
                                        <option value="04.00pm">04.00 pm</option>
                                        <option value="04.15pm">04.15 pm</option>
                                        <option value="04.30pm">04.30 pm</option>
                                        <option value="04.45pm">04.45 pm</option>
                                        <option value="05.00pm">05.00 pm</option>
                                        <option value="05.15pm">05.15 pm</option>
                                        <option value="05.30pm">05.30 pm</option>
                                        <option value="05.45pm">05.45 pm</option>
                                        <option value="06.00pm">06.00 pm</option>
                                        <option value="06.15pm">06.15 pm</option>
                                        <option value="06.30pm">06.30 pm</option>
                                        <option value="06.45pm">06.45 pm</option>
                                        <option value="07.00pm">07.00 pm</option>
                                        <option value="07.15pm">07.15 pm</option>
                                        <option value="07.30pm">07.30 pm</option>
                                        <option value="07.45pm">07.45 pm</option>
                                        <option value="08.00pm">08.00 pm</option>
                                        <option value="08.15pm">08.15 pm</option>
                                        <option value="08.30pm">08.30 pm</option>
                                        <option value="08.45pm">08.45 pm</option>
                                        <option value="09.00pm">09.00 pm</option>
                                        <option value="09.15pm">09.15 pm</option>
                                        <option value="09.30pm">09.30 pm</option>
                                        <option value="09.45pm">09.45 pm</option>
                                        <option value="10.00pm">10.00 pm</option>
                                        <option value="10.15pm">10.15 pm</option>
                                        <option value="10.30pm">10.30 pm</option>
                                        <option value="10.45pm">10.45 pm</option>
                                        <option value="11.00pm">11.00 pm</option>
                                        <option value="11.15pm">11.15 pm</option>
                                    </select>
                                    <div class="empty-sm-20  empty-xs-20"></div>
                                </div>
    
                                <div class="col-md-4">
                                    <label for="amount">Total Person</label>
                                    <input type="text" class="input-field" placeholder="1" name="amount" required="">
                                    <div class="empty-sm-20  empty-xs-20"></div>
                                </div>
    
                                <div class="col-md-6">
                                    <label for="email">Email</label>
                                    <input type="email" class="input-field" placeholder="jhon.doe@gmail.com" name="email" required="">
                                    <div class="empty-sm-20  empty-xs-20"></div>
                                </div>
    
                                <div class="col-md-6">
                                    <label for="phone">Contact No.</label>
                                    <input type="text" class="input-field" placeholder="+44 161 819 1187" name="phone" required="">
                                    <div class="empty-sm-20  empty-xs-20"></div>
                                </div>
    
    
                                <div class="col-md-12">
                                    <div class="input-field-icon">
                                        <label for="message">Message</label>
                                        <textarea class="input-field" placeholder="Message" name="message"></textarea>
                                    </div>
                                </div>
                                <div class="empty-sm-30  empty-xs-20"></div>
                                <div class="page-button button-style-1 type-2">
                                    <input type="submit" id="submit-btn">
                                    <span class="txt">BOOK A TABLE</span><i></i>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="empty-lg-100 empty-md-100 empty-sm-60 empty-xs-60"></div>
        </section>
    
        </div>
    
@endsection

@section('scripts')
    {{--  External Javascript  --}}
    <script src="{{asset('frontend/js/nice-select.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script>
        $(document).ready(function() {
            $('select').niceSelect();
        });
    </script>
    <script>
        // Inline Javascript Here
        window.onload = function() {
            $('.header-menu .reservation').addClass('active');
        };
    </script>
@endsection